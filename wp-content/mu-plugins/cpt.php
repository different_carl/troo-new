<?php
/* 
Plugin Name: Custom Post Types
Description: Custom Post Types registered for Troo
Author: everythingdifferent
Version: 1.0
Author URI: http://everythingdifferent.co.uk
*/

// Flush rewrite rules for custom post types
add_action( 'after_switch_theme', 'the_theme_flush' );

// Flush your rewrite rules
function the_theme_flush() {
	flush_rewrite_rules();
}

add_action( 'init', 'advice_cpt' );

/**
 * Register a advice post type.
 *
 * @link http://codex.wordpress.org/Function_Reference/register_post_type
 */
function advice_cpt() {
	register_post_type( 'advice', /* (http://codex.wordpress.org/Function_Reference/register_post_type) */
		// let's now add all the options for this post type
		array('labels' => array(
			'name' => __('Energy Advice', 'jointswp'), /* This is the Title of the Group */
			'singular_name' => __('Advice Article', 'jointswp'), /* This is the individual type */
			'all_items' => __('All Advice Articles', 'jointswp'), /* the all items menu item */
			'add_new' => __('Add New', 'jointswp'), /* The add new menu item */
			'add_new_item' => __('Add New Advice Article', 'jointswp'), /* Add New Display Title */
			'edit' => __( 'Edit', 'jointswp' ), /* Edit Dialog */
			'edit_item' => __('Edit Advice Articles', 'jointswp'), /* Edit Display Title */
			'new_item' => __('New Advice Article', 'jointswp'), /* New Display Title */
			'view_item' => __('View Advice Articles', 'jointswp'), /* View Display Title */
			'search_items' => __('Search Advice Articles', 'jointswp'), /* Search Custom Type Title */ 
			'not_found' =>  __('Nothing found in the Database.', 'jointswp'), /* This displays if there are no entries yet */ 
			'not_found_in_trash' => __('Nothing found in Trash', 'jointswp'), /* This displays if there is nothing in the trash */
			'parent_item_colon' => ''
			), /* end of arrays */
			'description' => __( 'This is the energy advice articles section', 'jointswp' ), /* Custom Type Description */
			'public' => true,
			'publicly_queryable' => true,
			'exclude_from_search' => false,
			'show_ui' => true,
			'query_var' => true,
			'menu_position' => 8, /* this is what order you want it to appear in on the left hand side menu */ 
			'menu_icon' => 'dashicons-admin-site', /* the icon for the custom post type menu. uses built-in dashicons (CSS class name) */
			'rewrite'	=> array( 'slug' => 'advice', 'with_front' => false ), /* you can specify its url slug */
			'has_archive' => 'advice', /* you can rename the slug here */
			'capability_type' => 'post',
		    'capabilities' => array(
		        'edit_post' => 'edit_advice',
		        'edit_posts' => 'edit_advices',
		        'edit_others_posts' => 'edit_other_advices',
		        'edit_published_posts' => 'edit_published_advices',
		        'publish_posts' => 'publish_advices',
		        'read_post' => 'read_advice',
		        'read_private_posts' => 'read_private_advices',
		        'delete_post' => 'delete_advice'
		    ),
		    // as pointed out by iEmanuele, adding map_meta_cap will map the meta correctly 
		    'map_meta_cap' => true,
			'hierarchical' => false,
			/* the next one is important, it tells what's enabled in the post editor */
			'supports' => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'sticky', 'custom-fields', 'revisions')
		) /* end of options */
	); /* end of register post type */
	
	/* this adds your post categories to your custom post type */
	// register_taxonomy_for_object_type('category', 'advice');
	/* this adds your post tags to your custom post type */
	register_taxonomy_for_object_type('post_tag', 'advice');
	
}

add_action( 'init', 'press_cpt' );

/**
 * Register a advice post type.
 *
 * @link http://codex.wordpress.org/Function_Reference/register_post_type
 */
function press_cpt() {
	register_post_type( 'press', /* (http://codex.wordpress.org/Function_Reference/register_post_type) */
		// let's now add all the options for this post type
		array('labels' => array(
			'name' => __('Press Releases', 'jointswp'), /* This is the Title of the Group */
			'singular_name' => __('Press Release', 'jointswp'), /* This is the individual type */
			'all_items' => __('All Press Releases', 'jointswp'), /* the all items menu item */
			'add_new' => __('Add New', 'jointswp'), /* The add new menu item */
			'add_new_item' => __('Add New Press Release', 'jointswp'), /* Add New Display Title */
			'edit' => __( 'Edit', 'jointswp' ), /* Edit Dialog */
			'edit_item' => __('Edit Press Releases', 'jointswp'), /* Edit Display Title */
			'new_item' => __('New Press Release', 'jointswp'), /* New Display Title */
			'view_item' => __('View Press Releases', 'jointswp'), /* View Display Title */
			'search_items' => __('Search Press Releases', 'jointswp'), /* Search Custom Type Title */ 
			'not_found' =>  __('Nothing found in the Database.', 'jointswp'), /* This displays if there are no entries yet */ 
			'not_found_in_trash' => __('Nothing found in Trash', 'jointswp'), /* This displays if there is nothing in the trash */
			'parent_item_colon' => ''
			), /* end of arrays */
			'description' => __( 'This is the energy Press Releases section', 'jointswp' ), /* Custom Type Description */
			'public' => true,
			'publicly_queryable' => true,
			'exclude_from_search' => false,
			'show_ui' => true,
			'query_var' => true,
			'menu_position' => 8, /* this is what order you want it to appear in on the left hand side menu */ 
			'menu_icon' => 'dashicons-admin-post', /* the icon for the custom post type menu. uses built-in dashicons (CSS class name) */
			'rewrite'	=> array( 'slug' => 'press-centre', 'with_front' => false ), /* you can specify its url slug */
			'has_archive' => 'press-centre', /* you can rename the slug here */
			'capability_type' => 'post',
		    'capabilities' => array(
		        'edit_post' => 'edit_press',
		        'edit_posts' => 'edit_presses',
		        'edit_others_posts' => 'edit_other_presses',
		        'edit_published_posts' => 'edit_published_presses',
		        'publish_posts' => 'publish_presses',
		        'read_post' => 'read_press',
		        'read_private_posts' => 'read_private_presses',
		        'delete_post' => 'delete_press'
		    ),
		    // as pointed out by iEmanuele, adding map_meta_cap will map the meta correctly 
		    'map_meta_cap' => true,
			'hierarchical' => false,
			/* the next one is important, it tells what's enabled in the post editor */
			'supports' => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'sticky', 'custom-fields', 'revisions')
		) /* end of options */
	); /* end of register post type */
	
	/* this adds your post categories to your custom post type */
	// register_taxonomy_for_object_type('category', 'advice');
	/* this adds your post tags to your custom post type */
	register_taxonomy_for_object_type('post_tag', 'press');
	
}

?>
