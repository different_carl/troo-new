<aside class="press-sidebar" role="complementary">
	
	<p class="show-for-medium"><strong>Find the latest press releases and download the troo press kit here.</strong></p>
	
	<hr class="show-for-medium" />
	
	<p class="align-swap">
		<?php if( get_field( 'press_contact', 'option' ) ): ?><strong>Contact:</strong> <?php echo get_field( 'press_contact', 'option' );?> <br /><?php endif;?>
		<strong>Tel:</strong> <?php get_template_part('parts/content-telephone', 'press'); ?><br />
		<strong>Email:</strong> <a href="mailto:<?php echo get_field( 'press_email_address', 'option' );?>"><?php echo get_field( 'press_email_address', 'option' );?></a>
	</p>
	
	<hr class="show-for-medium" />
	<?php if( get_field( 'press_about', 'option' ) ): ?>
	<?php echo get_field( 'press_about', 'option' );?> 
	<hr class="show-for-medium" />
	<?php endif;?>

	<?php if( have_rows('press_doc', 'option') ): ?>
	
		<p class="show-for-medium">
			<strong>Press Kit:</strong><br />
			<?php while ( have_rows('press_doc','option') ) : the_row(); ?>
			<?php echo get_sub_field('name') . '<a href="'. get_sub_field('file') .'" class="float-right" target="_blank">Download</a><br />';?>
			<?php endwhile;?>


		</p>
	<?php endif;?>
	
</aside><!-- // press-sidebar -->