<?php 

if( ! is_page_template('my-templates/iframe.php') ): 
	$a = "medium-7 medium-pull-5";
endif;
?>
	<footer class="footer" role="contentinfo">
		<div class="row">
		<div class="columns">
		

		<?php if( ! is_page_template('my-templates/iframe.php') ): ?>
			<div class="footer-cta small-12 medium-4 medium-offset-1 medium-push-7 columns">
				<h4>Want to pay less for your business energy?</h4>
				<?php
				 	$c = get_field('f_call_to_action','option');
				 	$i = get_field('f_link','option');
				 	if( empty($c) ): $c = 'sign me up';endif;
				 ?>
				<?php 
				if( $c ):
					echo '<a class="button ga--track--signup" data-g-a="Sign me up - button - footer" href="'. $i .'">'. $c .'</a>';
				endif;
				?>
				<p><strong>or call free: <?php get_template_part('parts/content', 'telephone'); ?></strong></p>
			</div><!-- // footer-cta -->
		<?php endif;?>

			<div class="contact_foot-links small-12 <?php echo $a;?> columns">
				<?php $o = get_field('f_open_times','option');?>
				<?php if( !empty($o) ): echo '<p>'.$o.'</p>';endif?>
				<?php joints_footer_links(); ?>
			</div><!-- // contact_foot-links -->

			<div class="soc-icons small-12 columns">
				<div class="soc-icons-bg">
				<?php $x = get_field( 'show_social', 'option' ) ?>
				
				<?php if($x == true){ ?>
							<?php
							// check if the repeater field has rows of data
							if( have_rows('social_media','option') ):

							 	// loop through the rows of data
							    while ( have_rows('social_media','option') ) : the_row();

							        // display a sub field value
							        $value = get_sub_field('which');
							        
							        $class = $value['value'];
							        $text = $value['label'];
									$link = get_sub_field( 'link_to_profile', 'option' );

							?>
							<a href="<?php echo $link; ?>" target="_blank">
								<?php get_template_part('assets/svg/icon', $class.'.svg'); ?>
							</a>
							<?php
							    endwhile;

							else :

							    // no rows found

							endif;

							?>
				<?php }; ?>
					
				</div>
			</div><!-- // soc-icons -->

		</div>
		</div>
	</footer><!-- // footer -->
</div><!-- // off-canvas-wrapper -->

<?php wp_footer(); ?>

<script type="text/javascript">
jQuery(document).ready(function($) {
	
		// when the plugin Ajax Load More completes a load...
		// $.fn.almComplete = function(alm){
		// 	console.log("Ajax Load More Complete!"); 

			
		// };
		// $('#ajax-load-more').foundation('getHeightsByRow', cb);

		// $(document).foundation('equalizer', 'reflow'); 
		
		// Foundation.reInit('equalizer');
		// new Foundation.Equalizer($('#ajax-load-more')).getHeights(); 

		// $('#ajax-load-more').foundation('getHeights', cb);

		// var fooEq = new Foundation.Equalizer($("#ajax-load-more"));
		// fooEq.getHeights(); 
		// fooEq.applyHeight(); 
		// fooEq.applyHeightByRow(); 

		// Foundation.reInit('equalizer');
		// $('#ajax-load-more').foundation();
		// new Foundation.Equalizer($("#ajax-load-more")).getHeights(); 

});</script>

</body>
</html>