<?php
/*
Template Name: About
*/
?>

<?php get_header(); ?>


<?php get_template_part('parts/content', 'feat-img'); ?>

<section class="page-title">

	<h1><?php the_title();?></h1>

</section><!-- // page-banner -->


<?php get_template_part('parts/content', 'breadcrumbs'); ?>


<section class="about-intro row">
	<?php if( get_field('about_headline') ):?>
		<div class="small-10 small-centered medium-8 large-6 medium-uncentered columns">
			<?php echo '<h2 class="xxl">'.get_field('about_headline').'</h2>';?>
		</div>
	<?php endif;?>

	<?php if( get_field('about_text') ):?>
	<div class="small-10 small-centered medium-4 large-6 medium-uncentered columns">
		<?php the_field('about_text');?>
	</div>
	<?php endif;?>

	<?php if( get_field( 'into_call_to_action' ) ): ?>
		<div class="centered-cta small-12 medium-12 columns">
			<a class="button ga--track--signup" data-g-a="Sign me up - button - header" href="<?php echo get_field('intro_link' );?>"><?php the_field('into_call_to_action'); ?></a>
			<p class="show-for-small-only">or call free: <?php get_template_part('parts/content', 'telephone'); ?></p>
		</div><!-- // centered-cta -->
	<?php endif; ?>

</section><!-- // about-intro -->


<section class="broker-compare">

	<?php if( get_field('brokervstroo_headline') ):?>
		<div class="row">
			<div class="small-offset-1 small-10 medium-12 medium-offset-0 column">
				<h2 class="title"><?php the_field('brokervstroo_headline');?></h2>
			</div>
		</div><!-- // row -->
	<?php endif;?>



	<div class="row">
		<div class="small-offset-1 small-10 medium-offset-0 medium-6 columns">

			<div class="compare-list compare-list-brokers">
				<?php if( get_field('brokervstroo_broker_title') ):?><p class="one"><span><?php the_field('brokervstroo_broker_title');?></p></span><?php endif;?>
				<?php if( get_field('broker_text_one') ):?><p class="equal two"><span><?php the_field('broker_text_one');?></p></span><?php endif;?>
				<?php if( get_field('broker_text_two') ):?><p class="equal three"><span><?php the_field('broker_text_two');?></p></span><?php endif;?>
				<?php if( get_field('broker_text_three') ):?><p class="equal four"><span><?php the_field('broker_text_three');?></p></span><?php endif;?>
				<?php if( get_field('broker_text_four') ):?><p class="equal five"><span><?php the_field('broker_text_four');?></p></span><?php endif;?>
				<?php if( get_field('broker_text_five') ):?><p class="equal six"><span><?php the_field('broker_text_five');?></p></span><?php endif;?>
			</div><!-- // compare-list compare-list-brokers -->
			
		</div>
		<div class="small-offset-1 small-10 medium-offset-0 medium-6 columns">
			
			<div class="compare-list compare-list-troo">
				<?php if( get_field('brokervstroo_troo_title') ):?><p class="one"><span><?php the_field('brokervstroo_troo_title');?></span></p><?php endif;?>
				<?php if( get_field('troo_text_one') ):?><p class="equal two"><span><?php the_field('troo_text_one');?></span></p><?php endif;?>
				<?php if( get_field('troo_text_two') ):?><p class="equal three"><span><?php the_field('troo_text_two');?></span></p><?php endif;?>
				<?php if( get_field('troo_text_three') ):?><p class="equal four"><span><?php the_field('troo_text_three');?></span></p><?php endif;?>
				<?php if( get_field('troo_text_four') ):?><p class="equal five"><span><?php the_field('troo_text_four');?></span></p><?php endif;?>
				<?php if( get_field('troo_text_five') ):?><p class="equal six"><span><?php the_field('troo_text_five');?></span></p><?php endif;?>
			</div><!-- // compare-list compare-list-brokers -->

		</div>
	</div><!-- // row -->

	<div class="row">
			<?php if( get_field( 'broker_call_to_action' ) ): ?>
		<div class="centered-cta small-12 medium-12 columns">
			<a class="button ga--track--signup" data-g-a="Start saving - button - broker" href="<?php echo get_field('broker_link' );?>"><?php the_field('broker_call_to_action'); ?></a>
			<p class="show-for-small-only">or call free: <?php get_template_part('parts/content', 'telephone'); ?></strong></p>
		</div><!-- // centered-cta -->
	<?php endif; ?>
	</div><!-- // row -->

</section><!-- // broker-compare -->


<?php get_template_part('parts/section', 'three-icons'); ?>


<?php get_footer(); ?>