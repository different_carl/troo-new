<?php
/*
Template Name: Home
*/
?>

<?php get_header(); ?>


<section class="home-hero valign-middle">
	<div class="vmiddle">
	<div class="row">

		<div class="small-12 medium-8 columns">
			
			<div class="intro">
				<?php if( get_field( 'home_headline' ) ): ?>
				<h1 class="xxl"><?php the_field('home_headline'); ?></h1>
				<?php endif; ?>

				<?php if( get_field( 'home_introduction' ) ): ?>
				<p><?php the_field('home_introduction'); ?></p>
				<?php endif; ?>
			</div><!-- // intro -->

		</div>
		<div class="small-12 medium-4 columns">
			
			<div class="intro-cta">
				<?php if( get_field( 'lead_text' ) ): ?>
				<a class="button ga--track--signup" data-g-a="Start saving - button - header" href="<?php the_field('lead_link'); ?>">
					<?php the_field('lead_text'); ?>
				</a>
				
				<p><strong>or call free: <?php get_template_part('parts/content', 'telephone'); ?></strong></p>
				<?php endif; ?>
			</div><!-- // intro-cta -->

		</div>
		</div>

	</div><!-- // row -->

</section><!-- // home-hero -->


<?php get_template_part('parts/section', 'three-icons'); ?>


<section class="troo-cta">
	<div class="row">

		<div class="small-12 columns">
			<?php if( get_field( 'callout_headline' ) ): ?>
				<h2><?php the_field('callout_headline'); ?></h2>
				<?php endif; ?>
			<a href="<?php the_field('callout_link'); ?>" class="button" ><?php the_field('callout_text'); ?></a>
		</div>
	</div><!-- // row -->
</section><!-- // troo-cta -->


<section class="troo-process">
	<div class="row">

		<?php if( get_field( 'process_headline' ) ): ?>
		<h2 class="title"><?php the_field('process_headline'); ?></h2>
		<?php endif; ?>

		<div class="troo-process-row">

			<div class="troo-process-col small-12 medium-4 columns">
				<p class="number-rounded">1</p>
				
				<?php if( get_field( 'process_step_1_headline' ) ): ?>
				<h3><?php the_field('process_step_1_headline'); ?></h3>
				<?php endif; ?>

				<?php if( get_field( 'process_step_1_text' ) ): ?>
				<p><?php the_field('process_step_1_text'); ?></p>
				<?php endif; ?>
			</div><!-- // troo-process-col -->

			<div class="troo-process-col small-12 medium-4 columns">
				<p class="number-rounded">2</p>

				<?php if( get_field( 'process_step_2_headline' ) ): ?>
				<h3><?php the_field('process_step_2_headline'); ?></h3>
				<?php endif; ?>

				<?php if( get_field( 'process_step_2_text' ) ): ?>
				<p><?php the_field('process_step_2_text'); ?></p>
				<?php endif; ?>
			</div><!-- // troo-process-col -->

			<div class="troo-process-col small-12 medium-4 columns">
				<p class="number-rounded">3</p>

				<?php if( get_field( 'process_step_3_headline' ) ): ?>
				<h3><?php the_field('process_step_3_headline'); ?></h3>
				<?php endif; ?>

				<?php if( get_field( 'process_step_3_text' ) ): ?>
				<p><?php the_field('process_step_3_text'); ?></p>
				<?php endif; ?>
			</div><!-- // troo-process-col -->

		</div><!-- // troo-process-row -->

		<div class="centered-cta small-12 medium-12 columns">
			<?php if( get_field( 'process_call_to_action' ) ): ?>
				<a class="button ga--track--signup" data-g-a="Start saving now - button" href="<?php echo get_field('process_link' );?>"><?php the_field('process_call_to_action'); ?></a>
			<?php endif; ?>
			
			<p class="show-for-small-only">or call free: <?php get_template_part('parts/content', 'telephone'); ?></strong></p>
		</div><!-- // centered-cta -->

	</div><!-- // row -->
</section><!-- // troo-process -->




<section class="troo-fairway-contrast troo-cta">
	<?php if( get_field( 'whoweare_headline' ) ): ?>
	<h2 class="title"><?php the_field('whoweare_headline'); ?></h2>
	<?php endif; ?>
</section><!-- // troo-fairway-contrast -->
<section class="troo-fairway">
	<div class="row">
		
		<div class="small-10 small-centered medium-11 large-10 columns">
			<?php if( get_field( 'whoweare_text' ) ): ?>
			<p><?php the_field('whoweare_text'); ?></p>
			<?php $c = get_field('whoweare_cta'); ?>
			<?php if(empty($c)):$c="Start saving";endif; ?>
			<?php $l = get_field('whoweare_link'); ?>

			<?php if($l):echo '<p><a href="'. $l .'" class="ga--track--signup" data-g-a="Start saving - text link">'. $c .'</a></p>'; endif;?>
			<?php endif; ?>
		</div>

	</div>
</section><!-- // troo-fairway -->


<section class="featured-news section-article">
	<div class="row medium-uncollapse">

		<?php 
			global $post; // required
			$args = array('posts_per_page'=>3,'post_type'=>'advice'); 
			$custom_posts = get_posts($args);
			$class = the_count($custom_posts);
			foreach($custom_posts as $post) : setup_postdata($post); 
		?>

		<article id="post-<?php the_ID(); ?>" <?php post_class('small-12 '.$class.' columns'); ?> role="article" data-equalizer-watch>
			<?php get_template_part( 'parts/loop', 'home' ); ?>
		</article><!-- // article -->
		
		<?php endforeach; ?>

	</div>
</section><!-- // featured-news -->


<?php get_footer(); ?>