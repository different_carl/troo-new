<?php
/*
Template Name: iFrame
*/
?>

<?php get_header(); ?>

<div class="go-back">
	<div class="row">
		<div class="small-10 small-centered medium-12 columns text-center">
		<script>
		  var homeUrl = '<?= get_home_url(); ?>';
		  var ref = document.referrer;
		  var cur = window.location.href;

		  if (ref != '') document.write('<a href="' + ref + '">&#8249; Go Back</a>') ; else document.write('<a href="' + homeUrl + '">&#8249; Go home</a>')
		</script>

		</div>
	</div>
</div>

<!-- <iframe class="signup" src="http://poc-troo.everythingdifferent.co.uk/" scroll="no"> -->
<iframe id="iFrameResizer0" class="signup temp-height" src="<?php echo get_field('iframe_url');?>" scroll="no">
  <p>Your browser does not support iframes.</p>
</iframe>

<?php get_footer(); ?>

<script>


var isOldIE = (navigator.userAgent.indexOf("MSIE") !== -1); // Detect IE10 and below
			iFrameResize({
				heightCalculationMethod : 'lowestElement',
				log                     : true,                  // Enable console logging
				// enablePublicMethods     : true,                  // Enable methods within iframe hosted page
				// checkOrigin				: false,
				// bodyScroll				: true,
				resizedCallback         : function(messageData){ // Callback fn when resize is received
					$('p#callback').html(
						'<b>1Frame ID:</b> '    + messageData.iframe.id +
						' <b>Height:</b> '     + messageData.height +
						' <b>Width:</b> '      + messageData.width + 
						' <b>Event type:</b> ' + messageData.type
					);
					$('#iFrameResizer0').removeClass('temp-height');
				},
				messageCallback         : function(messageData){ // Callback fn when message is received
					$('p#callback').html(
						'<b>2Frame ID:</b> '    + messageData.iframe.id +
						' <b>Message:</b> '    + messageData.message
					);
				},
				closedCallback         : function(id){ // Callback fn when iFrame is closed
					$('p#callback').html(
						'<b>3IFrame (</b>'    + id +
						'<b>) removed from page.</b>'
					);
				}
			});


		


</script>