<?php
/*
Template Name: Contact
*/
?>

<?php get_header(); ?>

<section class="page-banner">

<?php get_template_part('parts/content', 'feat-img'); ?>

</section><!-- // page-banner -->


<section class="page-title">

	<h1><?php the_field('m_headline');?></h1>

</section><!-- // page-banner -->


<?php get_template_part('parts/content', 'breadcrumbs'); ?>


<section class="contact-us row">

	<div class="small-10 small-centered medium-12 columns">

		<?php the_content();?>

		<div class="press-enquiry general-enquiry large-collapse">
			<div class="press-enquiry-title small-12 medium-3 large-4 columns">
				<h3>General enquiries</h3>
				<?php /* ?>
				<div class="triangle-down">
					<?php get_template_part('assets/svg/triangle', 'down.svg'); ?>
				</div>
				<?php */ ?>
			</div>
			<div class="press-enquiry-telephone small-12 medium-5 large-4 columns">
				<p>
					<span>
					Call free:<br />
					<?php get_template_part('parts/content', 'telephone'); ?>
					</span>
				</p>
			</div>
			<div class="press-enquiry-email small-12 medium-4 large-4 columns">
				<p>
					<span>
						Email:<br />
						<?php if( get_field( 'primary_email_address', 'option' ) ): ?>
					

								<a href="mailto:<?php echo get_field( 'primary_email_address', 'option' ) ;?>"><?php echo get_field( 'primary_email_address', 'option' );?></a>

						<?php endif;?>
					</span>
				</p>
			</div>
		</div><!-- // press-enquiry row -->


		<div class="press-enquiry large-collapse">
			<div class="press-enquiry-title small-12 medium-3 large-4 columns">
				<h3>Press enquiries</h3>
				<?php /* ?>
				<div class="triangle-down">
					<?php get_template_part('assets/svg/triangle', 'down.svg'); ?>
				</div>
				<?php */ ?>
			</div>
			<div class="press-enquiry-telephone small-12 medium-5 large-4 columns">
				<p>
					<span>
						Telephone:<br /><?php get_template_part('parts/content-telephone', 'press'); ?>

					</span>
				</p>
			</div>
			<div class="press-enquiry-email small-12 medium-4 large-4 columns">
				<p>
					<span>
						Email:<br />
						<?php if( get_field( 'press_email_address', 'option' ) ): ?>
					

								<a href="mailto:<?php echo get_field( 'press_email_address', 'option' ) ;?>"><?php echo get_field( 'press_email_address', 'option' );?></a>

						<?php endif;?>
					</span>
				</p>
			</div>
		</div><!-- // press-enquiry row -->
	
	</div>

</section><!-- // row -->


<?php get_footer(); ?>