<!doctype html>

<html class="no-js"  <?php language_attributes(); ?>>

<head>
	<meta charset="utf-8">
	

	<!-- Search console validation -->
	<meta name="google-site-verification" content="1BMWhzc7bqga6Mr7LqeyDaiCeq1cG1K59hfkgQFsp1k" />
	
	<!-- Force IE to use the latest rendering engine available -->
	<meta http-equiv="X-UA-Compatible" content="IE=edge">

	<!-- Mobile Meta -->
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta class="foundation-mq">
	<meta name="format-detection" content="telephone=no">
	<?php $og_ea=get_field('ea_seo_page_description','options');?>
	<?php $og_pr=get_field('pr_seo_page_description','options');?>
	<?php if( is_post_type_archive('advice') && !empty($og_ea) ): ?>
		<meta property="og:description" content="<?php echo $og_ea;?>">
		<?php elseif(is_post_type_archive('press') && !empty($og_pr) ): ?>
		<meta property="og:description" content="<?php echo $og_pr;?>">
	<?php endif;?>
	
	<?php if ( ! function_exists( 'has_site_icon' ) || ! has_site_icon() ) { ?>
		<!-- Icons & Favicons -->
		<link rel="icon" href="<?php echo get_template_directory_uri(); ?>/favicon.png">
		<link href="<?php echo get_template_directory_uri(); ?>/assets/img/apple-icon-touch.png" rel="apple-touch-icon" />
		<!--[if IE]>
			<link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/favicon.ico">
		<![endif]-->
		<meta name="msapplication-TileColor" content="#f01d4f">
		<meta name="msapplication-TileImage" content="<?php echo get_template_directory_uri(); ?>/assets/img/win8-tile-icon.png">
    	<meta name="theme-color" content="#121212">
    <?php } ?>

	<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">

	<?php wp_head(); ?>
	<!-- Drop Google Analytics here -->
	<script>
	  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

	  ga('create', 'UA-91331386-3', 'auto');
	  ga('send', 'pageview');



	</script>
	<!-- end analytics -->

	<!-- Facebook Pixel Code -->
	<script>
	!function(f,b,e,v,n,t,s)
	{if(f.fbq)return;n=f.fbq=function(){n.callMethod?
	n.callMethod.apply(n,arguments):n.queue.push(arguments)};
	if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
	n.queue=[];t=b.createElement(e);t.async=!0;
	t.src=v;s=b.getElementsByTagName(e)[0];
	s.parentNode.insertBefore(t,s)}(window,document,'script',
	'https://connect.facebook.net/en_US/fbevents.js');
	 	fbq('init', '494231084243533'); 
		fbq('track', 'PageView');
		fbq('track', 'ViewContent');
	</script>
	<noscript>
	 <img height="1" width="1" 
	src="https://www.facebook.com/tr?id=494231084243533&ev=PageView
	&noscript=1"/>
	</noscript>
	<!-- End Facebook Pixel Code -->

</head>

<!-- Uncomment this line if using the Off-Canvas Menu --> 
	
<body <?php body_class(); ?>>


<div class="off-canvas-wrapper">
							
	<?php get_template_part( 'parts/content', 'offcanvas' ); ?>
	
	<div class="off-canvas-content" data-off-canvas-content>
		
		<header class="header" role="banner">

			<div class="row">
				
				<?php get_template_part( 'parts/nav', 'topbar' ); ?>

			</div><!-- // row -->
	
		</header> <!-- end .header -->
