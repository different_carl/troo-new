<section class="page-banner advice-banner">

	<?php 
		if( function_exists( 'has_post_thumbnail' ) && has_post_thumbnail() ) { 
			$featuredImage = wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); 
	?>
	<div class="feat-img" style="background-image:url(<?php echo $featuredImage; ?>);"></div>
	<?php } else { ?>
	<!-- <div class="feat-img"></div> -->
	<?php } ?>

</section><!-- // page-banner -->


<?php get_template_part('parts/content', 'breadcrumbs'); ?>

<section class="single-energy-advice row">

	<article id="post-<?php the_ID(); ?>" <?php post_class('small-10 small-centered medium-8 columns'); ?> role="article" itemscope itemtype="http://schema.org/BlogPosting">
		<div class="text-center"><?php echo do_shortcode('[mashshare text="'.get_the_title().'"]'); ?></div>
		<header class="article-header">	
			<h1 class="entry-title small-text-left" itemprop="headline"><?php the_title(); ?></h1>
			<?php /* ?>
			<p class="date-posted"><small><?php the_time('d/m/y') ?></small></p>
			<?php */ ?>
		</header><!-- // article header -->

		<section class="entry-content" itemprop="articleBody">

			<?php the_content(); ?>
			<div class="text-center"><?php echo do_shortcode('[mashshare shares="false" buttons="true" align="right"]'); ?></div>
			<?php edit_post_link('&rarr; Edit this page', '<p class="author-admin">', '</p>'); ?>
		</section><!-- // article section -->

		<footer class="article-footer">

		</footer><!-- // article footer -->

	</article><!-- // article -->

</section>