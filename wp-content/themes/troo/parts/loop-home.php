
	
	<header class="article-header">
		
		<?php 
			$excerpt = get_field( "excerpt" );
			if( function_exists( 'has_post_thumbnail' ) && has_post_thumbnail() ) { 
				$featuredImage = wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); 
		?>
		<a href="<?php the_permalink() ?>" title="<?php the_title_attribute(); ?>" class="feat-img" style="background-image:url(<?php echo $featuredImage; ?>);"></a>
		<?php } else { ?>
		<!-- <a href="<?php the_permalink() ?>" title="<?php the_title_attribute(); ?>" ></a> -->
		<?php } ?>

		<h3><a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></h3>

	</header> <!-- end article header -->
	<section class="entry-content" itemprop="articleBody">

		<?php if($excerpt):?>
				<?php 
				echo '<p>'.$excerpt.'<a href="'. get_permalink($post->ID) . '" title="'. __('Read ', '') . get_the_title($post->ID).'">'. __(' Read more', '') .'</a></p>';
				?>
		<?php else: 
				echo '<p><a href="'. get_permalink($post->ID) . '" title="'. __('Read ', '') . get_the_title($post->ID).'">'. __(' Read more', '') .'</a></p>';
		?>
		<?php endif; ?>
		
	</section><!-- // entry-content -->
