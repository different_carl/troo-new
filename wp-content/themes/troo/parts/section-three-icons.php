
<section class="troo-icons">
	<div class="row">

		<?php if( get_field( 'reasons_headline' ) ): ?>
		<h2 class="title"><?php the_field('reasons_headline'); ?></h2>
		<?php endif; ?>



		
		<?php
		// check if the repeater field has rows of data
		if( have_rows('reason', 'option') ):

		 	// loop through the rows of data
		    while ( have_rows('reason','option') ) : the_row();
		?>
		<div class="troo-icons-col small-12 medium-4 columns">


	        <div class="icon-svg">
				<?php //the_sub_field('reason_icon'); 
				$icon = wp_get_attachment_image( get_sub_field('reason_icon'), 'medium');
				echo $icon;
				?>
			</div>

			<?php if( get_sub_field( 'reason_headline' ) ): ?>
			<h3><?php the_sub_field('reason_headline'); ?></h3>
			<?php endif; ?>
			
			<?php if( get_sub_field( 'reason_text' ) ): ?>
			<p><?php the_sub_field('reason_text'); ?></p>
			<?php endif; ?>

		</div><!-- // troo-icons-col -->
		<?php
		    endwhile;

		else :

		    // no rows found

		endif;


		?>

		<div class="centered-cta small-12 medium-12 columns">
			
			

			<?php if( get_field( 'text', 'option' ) ): ?>
				<a class="button ga--track--signup" data-g-a="Start saving - button - content" href="<?php echo get_field('link', 'option');?>"><?php the_field('text','option'); ?></a>
			<?php endif; ?>
			
			<p class="show-for-small-only">or call free: <?php get_template_part('parts/content', 'telephone'); ?></strong></p>
		</div><!-- // centered-cta -->

	</div><!-- // row -->
</section><!-- // troo-icons -->