<article id="post-<?php the_ID(); ?>" <?php post_class(''); ?> role="article">
	<?php $excerpt = get_field( "excerpt" );?>
	<header class="article-header">
		<h3><a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></h3>
		<p class="date-posted"><small><?php the_time('d/m/y') ?></small></p>
	</header> <!-- end article header -->

	<section class="entry-content" itemprop="articleBody">

		<?php if($excerpt):?>
				<?php 
				echo '<p>'.$excerpt.'<a href="'. get_permalink($post->ID) . '" title="'. __('Read ', '') . get_the_title($post->ID).'">'. __(' Read more', '') .'</a></p>';
				?>
		<?php else: 
				echo '<p><a href="'. get_permalink($post->ID) . '" title="'. __('Read ', '') . get_the_title($post->ID).'">'. __(' Read more', '') .'</a></p>';
		?>
		<?php endif; ?>
		
	</section><!-- // entry-content -->

</article><!-- // article -->