<article id="post-<?php the_ID(); ?>" <?php post_class(''); ?> role="article" itemscope itemtype="http://schema.org/BlogPosting">

	<header class="article-header">	
		<h1 class="entry-title" itemprop="headline"><?php the_title(); ?></h1>
		<p class="date-posted"><small><?php the_time('d/m/y') ?></small></p>
	</header><!-- // article header -->

	<section class="entry-content" itemprop="articleBody">
		<?php the_content(); ?>
		<?php edit_post_link('&rarr; Edit this page', '<p class="author-admin">', '</p>'); ?>
	</section><!-- // article section -->

	<footer class="article-footer row">
		
		<div class="press-enquiry medium-collapse">
			<div class="press-enquiry-title small-12 medium-3 columns">
				<h3>Press enquiries</h3>
				<?php /* ?>
				<div class="triangle-down">
					<?php get_template_part('assets/svg/triangle', 'down.svg'); ?>
				</div>
				<?php */ ?>
			</div>
			<div class="press-enquiry-telephone small-12 medium-5 columns">
				<p>
					<span>
					Telephone:<br />
					<?php get_template_part('parts/content', 'telephone'); ?>
					</span>
				</p>
			</div>
			<div class="press-enquiry-email small-12 medium-4 columns">
				<p>
					Email:<br />
					<a href="mailto:pr@troocost.com">pr@troocost.com</a>
				</p>
			</div>
		</div><!-- // press-enquiry row -->

	</footer><!-- // article footer -->

</article><!-- // article -->