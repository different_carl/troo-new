<!-- By default, this menu will use off-canvas for small
	 and a topbar for medium-up -->

<div class="top-bar" id="top-bar-menu">
	
	<div class="columns">
		<div class="float-left">
			<a class="logo" href="<?php echo home_url(); ?>" title="Home">
				<img src="<?php echo get_template_directory_uri(); ?>/assets/img/troo-logo.png" alt="<?php bloginfo('name'); ?> logo" />
			</a>
		</div>

		<div class="float-right show-for-small-only">
			<ul class="menu">
				<li><button class="menu-icon" type="button" data-toggle="off-canvas"></button></li>
			</ul>
		</div><!-- mobile upwards burger menu -->

		<div class="top-contact">
			<p class="nav-phone">
				<span class="call--free"><strong>call free:</strong></span>
				<?php get_template_part('parts/content', 'telephone'); ?>
			</p>
			

			<?php $x = get_field( 'show_opening', 'option' ) ?>
			<?php if($x == true):check_opening();endif; ?>
		</div><!-- // top-contact -->

		<?php if( ! is_page_template('my-templates/iframe.php') ): ?>
		
			<div class="primary-menu show-for-medium">
				<?php joints_top_nav(); ?>
			</div>
		<?php endif;?>
	</div>

</div><!-- // top-bar -->