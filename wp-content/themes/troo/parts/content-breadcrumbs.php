<div class="row">
	<div class="breadcrumb-cont small-10 small-offset-1 medium-offset-0 columns">
		<nav aria-label="You are here:" role="navigation">
			<?php if ( function_exists('yoast_breadcrumb') ) {
				yoast_breadcrumb('<p id="breadcrumbs" class="breadcrumbs">','</p>');
			} ?>
		</nav>
	</div><!-- // breadcrumbs -->
</div><!-- // row -->