<?php if( get_field( 'primary_telephone_number', 'option' ) ): ?>

	<?php 
	$data = get_field( 'primary_telephone_number', 'option' );
	$result = $data;
	// $result = substr($data, 0, 4)." ".substr($data, 4, 3)." ".substr($data,7,3)." ".substr($data,10,3);
	
	// $result = substr($data, 0, 4)." ".substr($data, 4, 3)." ".substr($data,7,4); //for 0800 numbers
	?>

	<span itemprop="telephone" class="numb">
		<a href="tel:<?php echo get_field( 'primary_telephone_number', 'option' ) ;?>"><?php echo $result;?></a>
	</span>

<?php endif;?>