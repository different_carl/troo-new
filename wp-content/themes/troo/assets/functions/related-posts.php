<?php
// Related Posts Function, matches posts by tags - call using joints_related_posts(); )
function joints_related_posts() {
	global $post;
	$tag_arr = '';
	$cpt = get_post_type( $post->ID );
	$tags = wp_get_post_tags( $post->ID );
	if($tags) {
		foreach( $tags as $tag ) {
			$tag_arr .= $tag->slug . ',';
		}
		$args = array(
			'post_type' => $cpt,
			'tag' => $tag_arr,
			'numberposts' => 2, /* you can change this to show more */
			'post__not_in' => array($post->ID)
		);
		$related_posts = get_posts( $args );
		
		$class = the_count($related_posts);


		if($related_posts) {
		echo '<div class="related-articles row">
				<div class="small-12 columns">';
				echo __( '<h2 class="text-center">Related articles</h2>', 'jointswp' );
				echo '<div class="row">';
				foreach ( $related_posts as $post ) : setup_postdata( $post ); ?>

						<article id="post-<?php the_ID(); ?>" <?php post_class('small-12 '.$class.' columns'); ?> role="article" data-equalizer-watch>
							<?php get_template_part( 'parts/loop', 'home' ); ?>
						</article><!-- // article -->
			<?php endforeach; 
		echo '</div></div>';

		}
	}
	wp_reset_postdata();
	echo '</div>';
} /* end joints related posts function */

function the_count($related_posts){
		$i = count($related_posts);
		switch ($i) {
		    case 1:
		    $class = 'medium-4';
		    // echo '1';
		        break;
		    case 2:
		    $class = 'medium-6';
		    // echo '2';
		        break;
		    case 3:
		    $class = 'medium-4';
		        break;
		    default:
		}

		return $class;
}

