<?php

// Fire all our initial functions at the start
add_action('after_setup_theme','joints_start', 16);

function joints_start() {

    // launching operation cleanup
    add_action('init', 'joints_head_cleanup');
    
    // remove pesky injected css for recent comments widget
    add_filter( 'wp_head', 'joints_remove_wp_widget_recent_comments_style', 1 );
    
    // clean up comment styles in the head
    add_action('wp_head', 'joints_remove_recent_comments_style', 1);
    
    // clean up gallery output in wp
    add_filter('gallery_style', 'joints_gallery_style');
    
    // adding sidebars to Wordpress
    add_action( 'widgets_init', 'joints_register_sidebars' );
    
    // cleaning up excerpt
    // add_filter('excerpt_more', 'joints_excerpt_more');

} /* end joints start */

//The default wordpress head is a mess. Let's clean it up by removing all the junk we don't need.
function joints_head_cleanup() {
	// Remove category feeds
	// remove_action( 'wp_head', 'feed_links_extra', 3 );
	// Remove post and comment feeds
	// remove_action( 'wp_head', 'feed_links', 2 );
	// Remove EditURI link
	remove_action( 'wp_head', 'rsd_link' );
	// Remove Windows live writer
	remove_action( 'wp_head', 'wlwmanifest_link' );
	// Remove index link
	remove_action( 'wp_head', 'index_rel_link' );
	// Remove previous link
	remove_action( 'wp_head', 'parent_post_rel_link', 10, 0 );
	// Remove start link
	remove_action( 'wp_head', 'start_post_rel_link', 10, 0 );
	// Remove links for adjacent posts
	remove_action( 'wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0 );
	// Remove WP version
	remove_action( 'wp_head', 'wp_generator' );
} /* end Joints head cleanup */

// Remove injected CSS for recent comments widget
function joints_remove_wp_widget_recent_comments_style() {
   if ( has_filter('wp_head', 'wp_widget_recent_comments_style') ) {
      remove_filter('wp_head', 'wp_widget_recent_comments_style' );
   }
}

// Remove injected CSS from recent comments widget
function joints_remove_recent_comments_style() {
  global $wp_widget_factory;
  if (isset($wp_widget_factory->widgets['WP_Widget_Recent_Comments'])) {
    remove_action('wp_head', array($wp_widget_factory->widgets['WP_Widget_Recent_Comments'], 'recent_comments_style'));
  }
}

// Remove injected CSS from gallery
function joints_gallery_style($css) {
  return preg_replace("!<style type='text/css'>(.*?)</style>!s", '', $css);
}

// This removes the annoying […] to a Read More link
// function joints_excerpt_more($more) {
// 	global $post;
// 	// edit here if you like
// return '<a href="'. get_permalink($post->ID) . '" title="'. __('Read ', '') . get_the_title($post->ID).'">'. __(' Read more', '') .'</a>';
// }

//Custom excerpt length on posts
// function custom_excerpt_length($length) {
// 	return 20;
// }
// add_filter('excerpt_length', 'custom_excerpt_length'); 

//  Stop WordPress from using the sticky class (which conflicts with Foundation), and style WordPress sticky posts using the .wp-sticky class instead
function remove_sticky_class($classes) {
	if(in_array('sticky', $classes)) {
		$classes = array_diff($classes, array("sticky"));
		$classes[] = 'wp-sticky';
	}
	
	return $classes;
}
add_filter('post_class','remove_sticky_class');

//This is a modified the_author_posts_link() which just returns the link. This is necessary to allow usage of the usual l10n process with printf()
function joints_get_the_author_posts_link() {
	global $authordata;
	if ( !is_object( $authordata ) )
		return false;
	$link = sprintf(
		'<a href="%1$s" title="%2$s" rel="author">%3$s</a>',
		get_author_posts_url( $authordata->ID, $authordata->user_nicename ),
		esc_attr( sprintf( __( 'Posts by %s', 'jointswp' ), get_the_author() ) ), // No further l10n needed, core will take care of this one
		get_the_author()
	);
	return $link;
}

/**
 * Rename "Posts" to "Press Releases"
 *
 * @link http://new2wp.com/snippet/change-wordpress-posts-post-type-news/
 */
// add_action( 'admin_menu', 'pilau_change_post_menu_label' );
// add_action( 'init', 'pilau_change_post_object_label' );
// function pilau_change_post_menu_label() {
// 	global $menu;
// 	global $submenu;
// 	$menu[5][0] = 'Press Releases';
// 	$submenu['edit.php'][5][0] = 'Press Releases';
// 	$submenu['edit.php'][10][0] = 'Add Press Release';
// 	$submenu['edit.php'][16][0] = 'Press Release Tags';
// 	echo '';
// }
// function pilau_change_post_object_label() {
// 	global $wp_post_types;
// 	$labels = &$wp_post_types['post']->labels;
// 	$labels->name = 'Press Release';
// 	$labels->singular_name = 'Press Release';
// 	$labels->add_new = 'Add Press Release';
// 	$labels->add_new_item = 'Add Press Release';
// 	$labels->edit_item = 'Edit Press Release';
// 	$labels->new_item = 'Press Release';
// 	$labels->view_item = 'View Press Release';
// 	$labels->search_items = 'Search Press Releases';
// 	$labels->not_found = 'No Press Releases found';
// 	$labels->not_found_in_trash = 'No Press Releases found in Trash';
// }

add_action('admin_menu','remove_default_post_type');

function remove_default_post_type() {
	remove_menu_page('edit.php');
}

add_action( 'admin_bar_menu', 'remove_wp_nodes', 999 );

function remove_wp_nodes() 
{
    global $wp_admin_bar;   
    $wp_admin_bar->remove_node( 'new-post' );

}


function ev_unregister_taxonomy(){
    register_taxonomy('category', array());
}
add_action('init', 'ev_unregister_taxonomy');

// FILTER TO CHANGE og:type conditionally
add_filter( 'wpseo_opengraph_type', 'yoast_change_opengraph_type', 10, 1 );
function yoast_change_opengraph_type( $type ) {
  /* Make magic happen here
   * Example below changes the homepage to a book type
   */
  
  if ( is_page() || is_post_type_archive() ) {
    return 'website';
  } else {
    return $type;
  }
}
