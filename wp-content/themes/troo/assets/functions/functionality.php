<?php

function check_opening(){
		
		if( get_field( 'sun_from', 'option' ) ):
		 $sunfrom =  get_field('sun_from', 'option');
		 $sunto =  get_field('sun_to', 'option');
		endif;
		
		if( get_field( 'monday_from', 'option' ) ):
		 $monfrom =  get_field('monday_from', 'option');
		 $monto =  get_field('monday_to', 'option');
		endif;
		
		if( get_field( 'tuesday_from', 'option' ) ):
		 $tuefrom =  get_field('tuesday_from', 'option');
		 $tueto =  get_field('tuesday_to', 'option');
		endif;
		
		if( get_field( 'wed_from', 'option' ) ):
		 $wedfrom =  get_field('wed_from', 'option');
		 $wedto =  get_field('wed_to', 'option');
		endif;
		
		if( get_field( 'thu_from', 'option' ) ):
		 $thufrom =  get_field('thu_from', 'option');
		 $thuto =  get_field('thu_to', 'option');
		endif;
		
		if( get_field( 'fri_from', 'option' ) ):
		 $frifrom =  get_field('fri_from', 'option');
		 $frito =  get_field('fri_to', 'option');
		endif;
		
		if( get_field( 'sat_from', 'option' ) ):
		 $satfrom =  get_field('sat_from', 'option');
		 $satto =  get_field('sat_to', 'option');
		endif;


		$storeSchedule = [
		    'Sun' => [$sunfrom => $sunto],
		    'Mon' => [$monfrom => $monto],
		    'Tue' => [$tuefrom => $tueto],
		    'Wed' => [$wedfrom => $wedto],
		    'Thu' => [$thufrom => $thuto],
		    'Fri' => [$frifrom => $frito],
		    'Sat' => [$satfrom => $satto]
		];

		$today_opening = array_keys($storeSchedule[date('D')]);
		$today_opening = $today_opening [0];
		$today_closing = array_values($storeSchedule[date('D')]);
		$today_closing = $today_closing [0];
		$tomorrow_opening = array_keys($storeSchedule[date('D', time()+24*60*60)]);
		$tomorrow_opening = $tomorrow_opening [0];
		$tomorrow_closing = array_values($storeSchedule[date('D', time()+24*60*60)]);
		$tomorrow_closing = $tomorrow_closing [0];

		if (strtotime("today " . $today_opening) > time())
		    $status = "<span class='nav-opening'>Opens at " . $today_opening . "</span>";
		elseif (strtotime("today " . $today_closing) > time())
		    $status = "<span class='nav-opening'>Open until " . $today_closing . "</span>";

		else
			$status = "<span class='nav-opening'>Open tomorrow ".$tomorrow_opening.'-'.$tomorrow_closing."</span>";

		echo $status;

		

}

// HIDE META BOXES
function remove_my_post_metaboxes() {
	   $post_types = get_post_types( '', 'names' );

	    if( ! empty( $post_types ) ) {
	        foreach( $post_types as $type ) {
	            remove_meta_box( 'mashsb_meta', $type, 'normal' );
	            remove_meta_box( 'revisionsdiv', $type, 'normal' );
	            remove_meta_box( 'postcustom', $type, 'normal' );
	            remove_meta_box( 'postexcerpt', $type, 'normal' );
	            remove_meta_box( 'authordiv', $type, 'normal' );
	        }
	    }
	// if( ! is_admin() ){

	// }
}
add_action('admin_head','remove_my_post_metaboxes', 999);



// ADDING CURRENT MENU ITEM CLASS TO ARCHIVE LINKS BASED ON URL
add_action('nav_menu_css_class', 'add_current_nav_class', 10, 2 );
	
	function add_current_nav_class($classes, $item) {
		
		// Getting the current post details
		global $post;
		
		// Getting the post type of the current post
		$current_post_type = get_post_type_object(get_post_type($post->ID));
		$current_post_type_slug = $current_post_type->rewrite[slug];
			
		// Getting the URL of the menu item
		$menu_slug = strtolower(trim($item->url));
		
		// If the menu item URL contains the current post types slug add the current-menu-item class
		if (strpos($menu_slug,$current_post_type_slug) !== false) {
		
		   $classes[] = 'current-menu-item';
		
		}
		
		// Return the corrected set of classes to be added to the menu item
		return $classes;
	
	}


