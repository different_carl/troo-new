jQuery(function ($) {

// blockquote hover sharing on articles
if($('body').is('.single')){

  
  var blockQuoteText;
  var currentUrl = window.location;



  $( "blockquote" ).each(function() {
    blockQuoteText = $(this).text();
     // add share buttons (change TwitterName to your Twitter handle for automatic mentions)
  // change paths to the icons to suit your installation (download link below)
  $(this).append('<div class="quote-share"><a target="_blank" class="social-icon quote-facebook facebook pop-up" href="http://www.facebook.com/sharer/sharer.php?u=' +currentUrl+ '&amp;title=' +blockQuoteText+ '">facebook</a><a target="_blank" class="social-icon quote-twitter twitter pop-up" href="https://twitter.com/intent/tweet?text=' +blockQuoteText+ '+' +currentUrl+ '+%40TrooCost">twitter</a></div>');
  });
  

 
} // end conditional for article pages

});