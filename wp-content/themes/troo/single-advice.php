<?php
/*
This is the custom post type post template.
If you edit the post type name, you've got
to change the name of this template to
reflect that name change.

i.e. if your custom post type is called
register_post_type( 'bookmarks',
then your single template should be
single-bookmarks.php

*/
?>

<?php get_header(); ?>

<main class="main-single" role="main">

	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

	<?php get_template_part( 'parts/loop', 'advice' ); ?>

	<?php endwhile; else : ?>

	<?php get_template_part( 'parts/content', 'missing' ); ?>

	<?php endif; ?>

</main><!-- end #main -->


<div class="main-single-cta row">
	<div class="centered-cta small-12 medium-12 columns">
		<a class="button" href="#">
			sign me up
		</a>
		<p class="show-for-small-only">or call free: <?php get_template_part('parts/content', 'telephone'); ?></p>
	</div>
</div><!-- // row -->




		<?php joints_related_posts(); ?>


<?php get_footer(); ?>